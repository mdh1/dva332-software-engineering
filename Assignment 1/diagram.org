#+begin_src plantuml :file G12Lab1_assignment_1.1.png
title Use case
left to right direction

actor "Gold member" as gold
actor "User" as user
actor "Guest" as guest

Package "Renting Scooter" {
usecase "Browse nearby scooters" as view
usecase "Register account" as register
usecase "Login" as login
usecase "Rent scooter" as rent
usecase "End ride" as end
usecase "Upgrade to gold membership" as upgrade
usecase "Reserve scooter in advance" as reserve
usecase "Pay monthly" as monthly

reserve .> rent : include

note right of rent
    By scanning QR code and
    check payment information.
end note
note right of end
    Calculate payment based on distance,
    and lock scooter at the same time.
    Also allow user to change payment
    information if wanted.
end note
note right of monthly
    Pay some amount of money per month
    and don't pay for individual rides.
end note
note right of register
    By filling out name, e-mail adress,
    payment information, etc.
end note
}

guest -- view
guest -- register
gold --|> user
gold -- reserve
gold -- monthly
user -- view
user -- login
user -- upgrade
user -- rent
user -- end
#+end_src
