package lab3.tests;

import static org.junit.Assert.*;

import java.util.InputMismatchException;

import org.junit.Test;

import lab3.bank.Account;

public class AccountTest {

	@Test
	public void testGetMaxOverdrawn() {
		Account myBankAccount = new Account(100.0, "SEK", 0.0);
		assertEquals("max overdrawn should be 0", 0.0, myBankAccount.getMaxOverdrawn(), 0);

		myBankAccount = new Account(100.0, "SEK", 100.0);
		assertEquals("max overdrawn should be 100", 100.0, myBankAccount.getMaxOverdrawn(), 0);

		myBankAccount = new Account(100.0, "SEK", 2000.0);
		assertEquals("max overdrawn should be 2000", 2000.0, myBankAccount.getMaxOverdrawn(), 0);
	}

	@Test
	public void testSetMaxOverdrawn() {
		Account myBankAccount = new Account(100.0, "SEK", 0.0);

		myBankAccount.setMaxOverdrawn(100.0);
		assertEquals("max overdrawn should be 100", 100.0, myBankAccount.getMaxOverdrawn(), 0);

		myBankAccount.setMaxOverdrawn(2000.0);
		assertEquals("max overdrawn should be 2000", 2000.0, myBankAccount.getMaxOverdrawn(), 0);
	}

	@Test
	public void testGetCurrency() {
		Account myBankAccount = new Account(100.0, "SEK", 0.0);
		assertEquals("SEK", myBankAccount.getCurrency());

		myBankAccount = new Account(20.0, "USD", 0.0);
		assertEquals("USD", myBankAccount.getCurrency());

		myBankAccount = new Account(20.0, "EUR", 0.0);
		assertEquals("EUR", myBankAccount.getCurrency());
	}

	@Test
	public void testSetCurrency() {
		Account myBankAccount = new Account();

		myBankAccount.setCurrency("EUR");
		assertEquals("EUR", myBankAccount.getCurrency());

		myBankAccount.setCurrency("USD");
		assertEquals("USD", myBankAccount.getCurrency());
	}

	@Test
	public void testSetBallance() {

		/*
		 * Since the balance and the currency are private methods, we cannot
		 * test get and set in isolation, therefore we just copy-paste the test
		 * cases. *
		 */

		Account myBankAccount = new Account(0.0, "EUR", 100.0);

		myBankAccount.setBalance(27.0);
		assertEquals("The value of the account shall be 27", 27.0, myBankAccount.getBalance(), 0);

		myBankAccount.setBalance(42.42);
		assertEquals("The value of the account shall be 42.42", 42.42, myBankAccount.getBalance(), 0);

		myBankAccount.setBalance(-200.0);
		assertEquals("When a too low value for the balance has been provided " + "the balance shall be set to the minimum.", -100, myBankAccount.getBalance(), 0);
	}

	@Test
	public void testGetBallance() {

		/*
		 * Since the balance and the currency are private methods, we cannot
		 * test get and set in isolation, therefore we just copy-paste the test
		 * cases. *
		 */
		Account myBankAccount = new Account();

		myBankAccount.setBalance(27.0);
		assertEquals("The value of the account shall be 27", 27.0, myBankAccount.getBalance(), 0);

		myBankAccount.setBalance(42.42);
		assertEquals("The value of the account shall be 42.42", 42.42, myBankAccount.getBalance(), 0);
	}

	@Test
	public void testAccount() {
		/*
		 * An account should be able to be created from both constructors,
		 * with all of it's member fields being present.
		 */
		Account testBankAccount = new Account();
		assertNotNull(testBankAccount);
		Account myBankAccount = new Account(100.0, "USD", 10.0);
		assertNotNull(myBankAccount);
		assertEquals("The value of the account shall be 100.0", 100.0, myBankAccount.getBalance(), 0);
		assertEquals("USD", myBankAccount.getCurrency());
		assertEquals("max overdrawn should be 10.0", 10.0, myBankAccount.getMaxOverdrawn(), 0);
	}

	@Test
	public void testWithdraw() {
		/*
		 * An account should be able to withdraw money and thus decreasing it's
		 * balance by that amount
		 */
		Account myBankAccount = new Account(42.42, "SEK", 100.0);
		
		myBankAccount.withdraw(42.42);
		assertEquals("The value of the account shall be 0.0", 0.0, myBankAccount.getBalance(), 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testWithdraw2() {
		/*
		 * BUG: Test resulted in subtracting a negative number, thus increasing the amount.
		 * Wanted behavior: Method should fail if a negative number is passed.
		 * Skeleton behavior: The negative number was added to the balance.
		 * Reason for input values: To test the behavior of a negative input value.
		 * FIX: Check if the input value is negative. If it is, throw an exception.
		 */
		Account myBankAccount = new Account(100.0, "SEK", 100.0);
		myBankAccount.withdraw(-100.0);
	}
	
	@Test
	public void testWithdraw3() {
		/*
		 * BUG: Test resulted in balance going under the allowed maxOverdrawn limit.
		 * Wanted behavior: The balance should be -maxOverdrawn, at the least.
		 * Skeleton behavior: Balance was decreased more than the allowed limit.
		 * Reason for input values: To check what happens when we try to go under the -maxOverdrawn limit.
		 * FIX: Check if (balance - withdrawAmount) is less than -maxOverdawn. If it is, set balance to -maxOverdrawn.
		 */
		Account myBankAccount = new Account(0.0, "SEK", 100.0);
		myBankAccount.withdraw(101.0);
		assertEquals("The value of the account shall be -100.0", -100.0, myBankAccount.getBalance(), 0);
	}

	@Test
	public void testDeposit() {
		/*
		 * An account should be able to deposit money and thus increasing
		 * it's balance by that amount.
		 */
		Account myBankAccount = new Account();
		
		myBankAccount.setBalance(0.0);
		myBankAccount.deposit(42.42);
		assertEquals("The value of the account shall be 42.42", 42.42, myBankAccount.getBalance(), 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testDeposit2() {
		/*
		 * BUG: Test resulted in 100.0 being subtracted from balance.
		 * Wanted behavior: Method should fail if a negative number is passed.
		 * Skeleton behavior: Negative numbers were added to balance.
		 * Reason for input values: To check if negative numbers can be added to balance.
		 * FIX: Check if input is a negative number. If it is, throw an exception.
		 */
		Account myBankAccount = new Account(100.0, "SEK", 100.0);
		myBankAccount.deposit(-100.0);
	}
	
	@Test
	public void testDeposit3() {
		/*
		 * BUG: Double arithmetic is rounded incorrectly/imprecise
		 * Wanted behavior: Balance should contain 10.8
		 * Skeleton behavior: Balance does no contain 10.8
		 * Reason for input values: To see if the doubles are rounded c
		 * FIX: Rounding the balance (with a preference for rounding up) from the third decimal (thousands),
		 * thus retaining the intended hundred decimals
		 */
		Account myBankAccount = new Account(0.0, "SEK", 100.0);
		myBankAccount.deposit(10.7);
		myBankAccount.deposit(0.1);
		assertEquals("The balance of the account shall be 10.8", 10.8, myBankAccount.getBalance(), 0.0);
	}

	@Test
	public void testConvertToCurrency() {
		/*
		 * An account should be able to convert it's balance to another currency
		 * with a fixed rate, thus multiplying it's own balance with that fixed rate.
		 * Furthermore, the chosen currency to convert to should then be that accounts
		 * currency when looking at it's 'currency' member field.
		 * maxOverdrawn should also be converted to the new currency.
		 */
		Account myBankAccount = new Account(10.0, "USD", 100.0);
		assertEquals("The value of the account shall be 10.0", 10.0, myBankAccount.getBalance(), 0);
		
		myBankAccount.convertToCurrency("SEK", 0.1);
		assertEquals("The value of the account shall be 1.0", 1.0, myBankAccount.getBalance(), 0);
		assertEquals("SEK", myBankAccount.getCurrency());
		assertEquals("The value of maxOverdrawn should be 10.0", 10.0, myBankAccount.getMaxOverdrawn(), 0);
		
		myBankAccount.convertToCurrency("USD", 10.0);
		assertEquals("The value of the account shall be 10.0", 10.0, myBankAccount.getBalance(), 0);
		assertEquals("USD", myBankAccount.getCurrency());
		assertEquals("The value of maxOverdrawn should be 100.0", 100.0, myBankAccount.getMaxOverdrawn(), 0);
		
		myBankAccount.convertToCurrency("EUR", 0.9);
		assertEquals("The value of the account shall be 9.0", 9.0, myBankAccount.getBalance(), 0);
		assertEquals("EUR", myBankAccount.getCurrency());
		assertEquals("The value of maxOverdrawn should be 90.0", 90.0, myBankAccount.getMaxOverdrawn(), 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConvertToCurrency2() {
		/*
		 * BUG: Test resulted in balance being multiplied with a negative rate, which is not intended.
		 * Wanted behavior: Method should fail if a negative rate is passed to it.
		 * Skeleton behavior: Balance was multiplied by the negative rate.
		 * Reason for input values: To check is negative numbers can be passed.
		 * FIX: Check if rate is negative. If it is, throw an exception.
		 */
		Account myBankAccount = new Account(100.0, "SEK", 100.0);
		myBankAccount.convertToCurrency("USD", -0.9);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConvertToCurrency3() {
		/*
		 * BUG: Balance was multiplied with 0, which should't be possible.
		 * Wanted behavior: Method should fail if rate of zero is passed to it.
		 * Skeleton behavior: Balance was multiplied by zero.
		 * Reason for input values: To check is zero can be passed.
		 * FIX: Check if rate is zero. If it is, throw an exception.
		 */
		Account myBankAccount = new Account(100.0, "SEK", 100.0);
		myBankAccount.convertToCurrency("BULL", 0.0);
	}

	@Test
	public void testTransferToAccount() {
		/*
		 * An account should be able to transfer all it's money to another account,
		 * thus increasing the target account balance by that amount and setting
		 * the original accounts balance to zero.
		 */
		Account myBankAccount = new Account();
		
		myBankAccount.setBalance(100.0);
		assertEquals("The value of the account shall be 100.0", 100.0, myBankAccount.getBalance(), 0);
		
		Account targetAccount = new Account();
		
		targetAccount.setBalance(50.0);
		assertEquals("The value of the account shall be 50.0", 50.0, targetAccount.getBalance(), 0);
		
		myBankAccount.TransferToAccount(targetAccount);
		assertEquals("The value of the account shall be 150.0", 150.0, targetAccount.getBalance(), 0);
		assertEquals("The value of the account shall be 0.0", 0.0, myBankAccount.getBalance(), 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testTransferToAccount2() {
		/*
		 * BUG: Transferring account balance to another account with a different currency
		 * 	worked, which isn't intended.
		 * Wanted behavior: Method should fail of the accounts have different currencies.
		 * Skeleton behavior: Balance of origin account was added to the target account, regardless of currency.
		 * Reason for input values: To check if it's possible to transfer money to an account of differing currency.
		 * FIX: Throw an exception of the two accounts have different currencies.
		 */
		Account myBankAccount = new Account(100.0, "SEK", 100.0);
		Account targetAccount = new Account(10.0, "EUR", 10.0);
		
		myBankAccount.TransferToAccount(targetAccount);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testTransferToAccount3() {
		/*
		 * BUG: Overdrawn money (negative double) was sent to the target account,
		 * 	which isn't intended.
		 * Wanted behavior: Method should fail of the first account has a negative balance.
		 * Skeleton behavior: The negative balance was added to the target account.
		 * Reason for input values: To check if an account with negative balance can transfer to another account.
		 * FIX: Throw an exception if the first account has a negative balance.
		 */
		Account myBankAccount = new Account(-100.0, "SEK", 100.0);
		Account targetAccount = new Account(10.0, "SEK", 10.0);
		
		myBankAccount.TransferToAccount(targetAccount);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testTransferToAccount4() {
		/*
		 * BUG: A bank account can transfer to itself, thus setting it's own balance to zero.
		 * Wanted behavior: A bank account should not be able to transfer to itself.
		 * Skeleton behavior: A bank account can transfer to itself.
		 * Reason for input values: To check if an account can transfer it's balance to itself.
		 * FIX: Throw an exception if targetAccount equals the calling object.
		 */
		Account myBankAccount = new Account(-100.0, "SEK", 100.0);
		myBankAccount.TransferToAccount(myBankAccount);
	}
}
