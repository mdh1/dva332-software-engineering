package lab3.bank;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Account implements IAccount {
	
	private Double balance;
	private String currency;
	private Double maxOverdrawn;
	
	public Double getMaxOverdrawn() {
		return this.maxOverdrawn;
	}
	
	public void setMaxOverdrawn(Double maxOverdrawn) {
		this.maxOverdrawn = maxOverdrawn;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setBalance(Double balance) {
		if(balance < -this.maxOverdrawn) {
			this.balance = -this.maxOverdrawn;
		}
		else {
			this.balance = balance;
		}
	}
	
	public Double getBalance() {
		return balance;
	}
	
	public Account() {
		balance = 0.0;
		currency = "SEK";
		maxOverdrawn = 0.0;
	}
	
	public Account(Double _amount, String _currency, Double _maxOverdrawn) {
		balance = _amount;
		currency = _currency;
		maxOverdrawn = _maxOverdrawn;
	}

	@Override
	public Double withdraw(Double requestedAmount) {
		if(requestedAmount < 0) {
			throw new IllegalArgumentException("Input cannot be negative");
		}
		
		if(this.balance - requestedAmount < -this.maxOverdrawn) {
			this.balance = -this.maxOverdrawn;
		}
		else {
			this.balance -= requestedAmount;
		}
		return this.balance;
	}

	@Override
	public Double deposit(Double amountToDeposit) {
		if(amountToDeposit < 0) {
			throw new IllegalArgumentException("Input cannot be negative");
		}
		
		balance += amountToDeposit;
		balance = roundPrecise(balance);
		return balance;
	}
	
	private Double roundPrecise(Double num) {
		return BigDecimal.valueOf(num).setScale(3, RoundingMode.HALF_UP).doubleValue();
	}

	@Override
	public void convertToCurrency(String currencyCode, Double rate) {
		if(rate <= 0) {
			throw new IllegalArgumentException("Rate cannot be negative");
		}
		currency = currencyCode;
		balance *= rate;
		maxOverdrawn *= rate;
	}

	@Override
	public void TransferToAccount(Account targetAccount) {
		if(balance < 0) {
			throw new IllegalArgumentException("Balance cannot be negative");
		}
		
		if(targetAccount == this) {
			throw new IllegalArgumentException("Source and target can not be the same account");
		}

		if(targetAccount.getCurrency() != this.currency) {
			throw new IllegalArgumentException("Accounts must be of same currency type");
		}
		
		targetAccount.deposit(this.balance);
		this.balance = 0.0;
	}
}
