package lab3.bank;

public interface IAccount {
	
	/*
	 * Method for withdrawing money from the account
	 * */
	public abstract Double withdraw(Double requestedAmount);	

	/*
	 * Method for adding money to account
	 * */
	public abstract Double deposit(Double amountToDeposit);
	
	/*
	 * Method for converting the account's money into a different currency
	 * */
	public abstract void convertToCurrency(String currencyCode, Double rate);
	
	/*
	 * Method that makes transfer from one account to another. 
	 * The transfer should be possible only if the accounts have the same currency 
	 * and the source account has a positive balance.
	 * */
	public abstract void TransferToAccount(Account targetAccount);
}
